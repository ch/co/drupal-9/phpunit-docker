To run phpunit tests via gitlab CI, include the provided template in your .gitlab-ci.yml. It provides a 'phpunit' job which defaults to the 'phpunit' stage:

```
include:
  - project: 'ch/co/drupal-9/phpunit-docker'
    file: '/templates/phpunit.yml'

stages:
  - phpunit
```

By default, phpunit tests will be run for the `--group` matching the repo project name. For example, if the repo is `ch/co/drupal/example_module`, the tests will be restricted to `--group example_module`. This can be overridden in the job inputs; see below.

Options
=======
stage: Stage to run the phpunit job in. Defaults to `phpunit`

phpunit_options: options to phpunit. Defaults to `--group $CI_PROJECT_NAME --testdox`

```
include:
 - project: 'ch/co/drupal-9/phpunit-docker'
   file: '/templates/phpunit.yml'
   inputs:
     stage: my_stage_name
     phpunit_options: --group mygroupname
```
