FROM registry.gitlab.developers.cam.ac.uk/ch/co/drupal-9/drupal9-ci-docker:minimal-v1.0.5

RUN sudo -u www-data composer -d /var/www/drupal/localhost require --dev drupal/core-dev:^10

COPY phpunit.xml /var/www/drupal/localhost

# need to set mysql password to match that in our phpunit.xml
RUN /bin/bash -c "/usr/bin/mysqld_safe  &" && \
    sleep 5 && \
    mysql -e "SET PASSWORD for 'localhost'@'localhost' = PASSWORD('phpunit')"

# then ensure that Drupal will use the same mysql password
RUN echo "\$databases['default']['default']['password'] = 'phpunit';" >> /var/www/drupal/localhost/web/sites/default/settings.php

ENTRYPOINT /root/start-service.sh && /bin/bash
